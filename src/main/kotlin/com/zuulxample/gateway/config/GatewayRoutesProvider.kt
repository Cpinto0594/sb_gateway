package com.zuulxample.gateway.config

import org.springframework.cloud.gateway.route.RouteLocator
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

@Configuration
class GatewayRoutesProvider {

    @Bean
    fun configRoutes(routelocatorBuilder: RouteLocatorBuilder): RouteLocator {
        return routelocatorBuilder.routes()
                .route("poki") { r ->
                    r.path("/poki/**")
                            .filters{
                                it.rewritePath("/poki/(?<segment>.*)", "/api/v1/$\\{segment}")
                            }
                            .uri("http://localhost:8081")

                }.build()
    }

}